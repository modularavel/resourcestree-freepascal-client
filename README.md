# ResourcesTree FreePascal Example Client

ResourcesTreeClient è un client REST API per l'applicazione Laravel ResourcesTree che fornisce un backend progettato per fornire principalmente API per l'interazione da parte dei client.

Questo client è sviluppato utilizzando [Lazarus/FreePascal](https://www.lazarus-ide.org/) per i seguenti motivi:

- Non sono amante di JavaScript e non sono aggiornato sulle tecnologie più avanzate per lo sviluppo di client integrati nel browser (es. Vue) e preferisco concentrare lo sforzo per costruire un backend affidabile invece di ottenere entrambi di scarsa qualità.

- Pascal è il mio linguaggio preferito, per quanto lo utilizzi esclusivamente per hobby.

## Licenza

Il client è distribuito con licenza [GNU General Public License v3.0](https://www.gnu.org/licenses/gpl-3.0.en.html).

## Download & Build

Il progetto è disponibile su GitLab e può essere scaricato con il comando:

```bash
git clone https://gitlab.com/modularavel/resourcestree-freepascal-client
```
### Requisiti

Per poter costruire il client sono necessari:

- [FreePascal](https://www.freepascal.org/) 3.2.2
- [Lazarus](https://www.lazarus-ide.org/) 2.2.6
- [JsonTools](https://github.com/sysrpl/JsonTools)
- [RestRequest4Delphi](https://github.com/viniciussanchez/RESTRequest4Delphi)

Per l'installazione dell'IDE consiglio l'utilizzo di [FpcUpDeluxe](https://github.com/LongDirtyAnimAlf/fpcupdeluxe)


## Il Backend

Il backend è costruito sul framework Laravel (PHP) e attivo sul [sito dimostrativo](https://resourcestree.inforisorse.it) dove è possibile accedere alla [documentazione API](https://resourcestree.inforisorse.it/api/documentation) nel formato standard _**Swagger**_.

Le API fornite dal backend permettono la gestione degli utenti fornendo le funzionalità:

- registrazione utente
- verifica email
- login
- logout
- visualizzazione profilo
- elenco profili (solo admin)

### API URL per le prove

Per poter provare il client si può utilizzare il sito di prova, impostando API Base Url con:


| campo  | valore |
|--------|--------|
| protocollo | https|
| host   | resourcestree.inforisorse.it |
| porta  | 80 |
| path | api/v1 |


Per poter accedere bisogna prima registrarsi (pulsante nella form di login), come identificativo viene utilizzato l'**indirizzo email** che ovviamente non deve già essere stato utilizzato; il campo _**Username (soprannome)**_ deve essere univoco e non viene utilizzato come identificativo quindi è l'equivalente di un _nickname_.

Per motivi di sicurezza il backend richiede la conferma dell'indirizzo email inviando un messaggio con il link all'indirizzo utilizzato nella registrazione; l'utente sarà attivato e potrà effettuare il login solamente dopo la conferma.


### Registrazione e verifica email

L'applicazione utilizza l'indirizzo email come identificatore univoco per gli utenti inoltre gli stessi, per poter operare devono effettuare la verifica dell'indirizzo email: dopo la registrazione viene inviata un'email di conferma all'indirizzo indicato, contenente il link per la verifica.

### Reset database ogni notte

Per evitare l'accumularsi di dati inutili, il database viene resettato ogni notte quindi tutti gli utenti ed i relativi dati saranno cancellati.

### Accesso ai servizi API

I servizi API sono accessibili da qualsiasi client REST, che sia sviluppato ad hoc con JavaScript od uno dei diversi framework in voga o che sia un tool classico come [Postman](https://www.postman.com/) o [Advanced REST Client](https://www.advancedrestclient.com/).

I client utilizzati devono essere in grado di effettuare le chiamate utilizzando non solo i verbi _**GET**_ e _**PUT**_.


## Il Client ResourcesTree

Il client è stato creato come caso di studio pratico per lo sviluppo di client REAT API con [FreePascal](https://www.freepascal.org/) e per testare le funzionalità API del backend. Per tale motivo è pittosto minimale e non sono state implementate funzionalità per il controllo formale, che è completamente demandato al backend in quanto responsabile in primis della validazione.

Questo non significa che i controlli formali non debbano essere aggiunti: se l'interfaccia intercetta degli errori, evitarà di effettuare delle chiamate inutili al backend, obbligando l'utente a correggere i dati prima di inviarli e riducendo quindi il consumo di risorse.

### Supporto

Il client è sviluppato in ambiente GNU/Linux. Non dovrebbe esserci alcun problema a compilare in ambiente Windows, nel quale dovrebbe essere richiesta la libreria dll per il protocollo SSL. Se avete bisogno di assistenza in ambiente Windows dovrete rivolgervi a qualcuno esperto nello sviluppo in quell'ambiente. Lo stesso vale per Delphi: eventuali adattamenti andranno sviluppati da qualcuno con le debite competenze e potranno esere eventualmente riportati nel progetto.

