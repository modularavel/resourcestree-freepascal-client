program ResourcesTreeClient;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}
  cthreads,
  {$ENDIF}
  {$IFDEF HASAMIGA}
  athreads,
  {$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, UMain, ULogin, URegisterProfile, DMMain, ResourcesTreeApiClient,
  UFrameTextFormField, UFrameLoginCredentials, UFrameUrl, ApiClientResponse,
  UFrameProfiledUserData, UUpdateProfile, UFrameProfileData, UFrameSetPassword,
  UFrameUserIdentifiers, UApiConfig
  { you can add units after this };

{$R *.res}

begin
  RequireDerivedFormResource := True;
  Application.Title := 'Resources Tree Client Sample';
  Application.Scaled := True;
  Application.Initialize;
  Application.CreateForm(TDataModuleMain, DataModuleMain);
  Application.CreateForm(TFormMain, FormMain);
  Application.CreateForm(TFormRegisterProfile, FormRegisterProfile);
  Application.CreateForm(TFormLogin, FormLogin);
  Application.CreateForm(TFormUpdateProfile, FormUpdateProfile);
  Application.Run;
end.

