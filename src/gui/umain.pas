unit UMain;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls,
  Buttons, UFrameProfiledUserData, ResourcesTreeApiClient, ApiClientResponse;

type

  { TFormMain }

  TFormMain = class(TForm)
    BtnExit: TBitBtn;
    BtnLogout: TBitBtn;
    BtnUpdateProfile: TBitBtn;
    FProfiledUserData: TFrameProfiledUserData;
    GroupBox1: TGroupBox;
    LabelRoles: TLabel;
    RolesList: TListBox;
    PanelTop: TPanel;
    PanelButtons: TPanel;
    procedure BtnLogoutClick(Sender: TObject);
    procedure BtnUpdateProfileClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
    FApiResponse: TApiClientResponse;
    function GetApiClient: TRTApiClient;
    procedure CheckLogin;
    procedure SetApiResponse(AValue: TApiClientResponse);
  private
    property ApiClient: TRTApiClient read GetApiClient;
    property ApiResponse: TApiClientResponse read FApiResponse write SetApiResponse;
  public

  end;

var
  FormMain: TFormMain;

implementation

uses DMMain, ULogin, UUpdateProfile, JsonTools, StrUtils;

{$R *.lfm}

{ TFormMain }

procedure TFormMain.FormShow(Sender: TObject);
begin
  CheckLogin;
end;

function TFormMain.GetApiClient: TRTApiClient;
begin
  Result := DataModuleMain.ApiClient;
end;

procedure TFormMain.CheckLogin;
var
  Node: TJsonNode;
  Role: TJsonNode;
  RoleList: string = '';
begin
  if not DataModuleMain.LoggedIn then
    begin
      if FormLogin.ShowModal = mrCancel then
      begin
        Close;
      end
      else
      begin
        with ApiClient do
        begin
          DataModuleMain.UserId := StrToInt(DataValue['id']);
          FProfiledUserData.Email :=  DataValue['email'];
          FProfiledUserData.Username := DataValue['name'];
          FProfiledUserData.FirstName := DataValue['first_name'];
          FProfiledUserData.LastName := DataValue['last_name'];
          FProfiledUserData.TaxId := DataValue['tax_id'];
          RolesList.Items.Clear;
          try
            Node := TJsonNode.Create;
            Node.Parse('{ ' + DataValue['roles'] + ' }');

            for Role in Node.Find('roles') do
            begin

              RolesList.Items.Add(StringReplace(Role.Value, '"', '', [rfReplaceAll]));
            end;

          finally
            FreeAndNil(Node);
          end;

        end;
      end;
    end;
end;

procedure TFormMain.SetApiResponse(AValue: TApiClientResponse);
begin
  if FApiResponse = AValue then Exit;
  FApiResponse := AValue;
end;

procedure TFormMain.BtnUpdateProfileClick(Sender: TObject);
begin

  FormUpdateProfile.Username := FProfiledUserData.Username;
  FormUpdateProfile.FirstName := FProfiledUserData.FirstName;
  FormUpdateProfile.LastName := FProfiledUserData.LastName;
  FormUpdateProfile.TaxId := FProfiledUserData.TaxId;

  if FormUpdateProfile.ShowModal = mrOK then
  begin
    with ApiClient do
    begin

      UpdateProfile(
        DataModuleMain.UserId,
        FormUpdateProfile.Username,
        FormUpdateProfile.FirstName,
        FormUpdateProfile.LastName,
        FormUpdateProfile.TaxId
      );

      FProfiledUserData.Username := DataValue['name'];
      FProfiledUserData.FirstName := DataValue['first_name'];
      FProfiledUserData.LastName := DataValue['last_name'];
      FProfiledUserData.TaxId := DataValue['tax_id'];
    end;
  end;

end;

{
  Ensure to close API sessione when exiting
}
procedure TFormMain.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  if ApiClient.LoggedIn then
  begin
    ApiClient.Logout;
  end;
end;

procedure TFormMain.BtnLogoutClick(Sender: TObject);
begin
  ApiClient.Logout;
  if not ApiClient.LoggedIn then
  begin
    CheckLogin;
  end;
end;

end.

