unit ULogin;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, Buttons,
  StdCtrls, UFrameLoginCredentials, UFrameUrl, UApiConfig, ApiClientResponse;

type

  { TFormLogin }

  TFormLogin = class(TForm)
    BitRegisterProfile: TBitBtn;
    BtnCancel: TBitBtn;
    BtnConnect: TBitBtn;
    FormLabel: TLabel;
    FLoginCredentials: TFrameLoginCredentials;
    UrlEditor: TFrameUrl;
    PanelTop: TPanel;
    PanelButtons: TPanel;
    procedure BtnConnectClick(Sender: TObject);
  private
    function GetApiConfig: TApiConfig;
  private
    function GetApiResponse: TApiClientResponse;
    procedure SetFormFieldsFromConfig;
    property ApiConfig: TApiConfig read GetApiConfig;
  protected
    procedure Loaded; override;
    property ApiResponse: TApiClientResponse read GetApiResponse;
  end;

var
  FormLogin: TFormLogin;

implementation

uses
  StrUtils,
  httpprotocol,
  DMMain,
  ResourcesTreeApiClient,
  URIParser;

{$R *.lfm}

{ TFormLogin }

procedure TFormLogin.BtnConnectClick(Sender: TObject);
begin
  ApiConfig.Username :=  FLoginCredentials.Email;
  ApiConfig.Password := FLoginCredentials.Password;
  ApiConfig.Host := UrlEditor.Host;
  ApiConfig.Path := UrlEditor.Path;
  ApiConfig.Port := StrToInt(UrlEditor.Port);
  ApiConfig.Protocol := UrlEditor.Protocol;

  if DataModuleMain.Login(
    ApiConfig.Username,
    ApiConfig.Password) then
  begin
    ApiConfig.SaveConfigValues;
    ModalResult := mrOK;
  end
  else
  begin
    ShowMessage(format('Login fallito.'#13#10#13#10' %s: "%s"',[
      floattostr(ApiResponse.Status),
      ApiResponse.Message
    ]));
  end;
end;

function TFormLogin.GetApiConfig: TApiConfig;
begin
  Result := DataModuleMain.ApiConfig;
end;

function TFormLogin.GetApiResponse: TApiClientResponse;
begin
  Result := DataModuleMain.ApiClient.ApiResponse;
end;

{
  Set form field values from API Config file
}
procedure TFormLogin.SetFormFieldsFromConfig;
begin
  UrlEditor.ErrorMessage := '';
  UrlEditor.Host := ApiConfig.Host;
  UrlEditor.Path := ApiConfig.BasePath;
  UrlEditor.Port := IntToStr(ApiConfig.Port);
  UrlEditor.Protocol := ApiConfig.Protocol;

  FLoginCredentials.EmailError := '';
  FLoginCredentials.Email := ApiConfig.Username;

  FLoginCredentials.PasswordError := '';
  FLoginCredentials.Password := ApiConfig.Password;
end;


procedure TFormLogin.Loaded;
begin
  inherited Loaded;
  UrlEditor.FieldLabel := 'API Base Url';
  SetFormFieldsFromConfig;
end;

end.
