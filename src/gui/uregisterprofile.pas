unit URegisterProfile;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ExtCtrls,
  Buttons, ComCtrls, LazHelpHTML,
  UFrameProfileData, UFrameSetPassword, UFrameUserIdentifiers,
  ResourcesTreeApiClient, ApiClientResponse;

type

  { TFormRegisterProfile }

  TFormRegisterProfile = class(TForm)
    BtnCancel: TBitBtn;
    BtnOK: TBitBtn;
    FProfileData: TFrameProfileData;
    FUserIdentifiers: TFrameUserIdentifiers;
    FSetPassword: TFrameSetPassword;
    LabelInfo: TLabel;
    PanelCredentials: TPanel;
    PanelProfileData: TPanel;
    PanelButtons: TPanel;
    Splitter1: TSplitter;
    procedure BtnOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    function GetApiClient: TRTApiClient;
    function GetApiResponse: TApiClientResponse;
    function GetEmail: String;
    function GetEmailError: String;
    function GetFirstName: String;
    function GetFirstNameError: String;
    function GetLastName: String;
    function GetLastNameError: String;
    function GetPassword: String;
    function GetPasswordConfirmation: String;
    function GetPasswordConfirmationError: String;
    function GetPasswordError: String;
    function GetTaxId: string;
    function GetTaxIdError: string;
    function GetUsername: String;
    function GetUsernameError: String;
    procedure SetEmail(AValue: String);
    procedure SetEmailError(AValue: String);
    procedure SetFirstName(AValue: String);
    procedure SetFirstNameError(AValue: String);
    procedure SetLastName(AValue: String);
    procedure SetLastNameError(AValue: String);
    procedure SetPassword(AValue: String);
    procedure SetPasswordConfirmation(AValue: String);
    procedure SetPasswordConfirmationError(AValue: String);
    procedure SetPasswordError(AValue: String);
    procedure SetTaxId(AValue: string);
    procedure SetTaxIdError(AValue: string);
    procedure SetUsername(AValue: String);
    procedure SetUsernameError(AValue: String);
  protected
    property ApiClient: TRTApiClient read GetApiClient;
    property ApiResponse: TApiClientResponse read GetApiResponse;
  public
    procedure CleanErrors;
    procedure CleanFields;
  published
    property Email: String read GetEmail write SetEmail;
    property EmailError: String read GetEmailError write SetEmailError;
    property FirstName: String read GetFirstName write SetFirstName;
    property FirstNameError: String read GetFirstNameError write SetFirstNameError;
    property LastName: String read GetLastName write SetLastName;
    property LastNameError: String read GetLastNameError write SetLastNameError;
    property Password: String read GetPassword write SetPassword;
    property PasswordError: String read GetPasswordError write SetPasswordError;
    property PasswordConfirmation: String read GetPasswordConfirmation write SetPasswordConfirmation;
    property PasswordConfirmationError: String read GetPasswordConfirmationError write SetPasswordConfirmationError;
    property TaxId: string read GetTaxId write SetTaxId;
    property TaxIdError: string read GetTaxIdError write SetTaxIdError;
    property Username: String read GetUsername write SetUsername;
    property UsernameError: String read GetUsernameError write SetUsernameError;
  end;

var
  FormRegisterProfile: TFormRegisterProfile;

implementation

uses RESTRequest4D, JsonTools, DMMain;


{$R *.lfm}

{ TFormRegisterProfile }

procedure TFormRegisterProfile.BtnOKClick(Sender: TObject);
var
  Result : Boolean;
  DataNode: TJsonNode;
  ErrorFieldNode: TJsonNode;

  function DisplayError(FieldName: string): String;
  begin
    ErrorFieldNode := DataNode.find(FieldName);
    if ErrorFieldNode <> nil then
    begin
      Result := StripQuotes(ErrorFieldNode.Child(0).Value);
    end
    else
    begin
      Result := '';
    end;
  end;

begin
  CleanErrors;

  Result := DataModuleMain.RegisterProfile(
    GetEmail,
    GetPassword,
    GetPasswordConfirmation,
    GetUsername,
    GetFirstName,
    GetLastName,
    GetTaxId
  );

  if DataModuleMain.ApiClient.ApiResponse.Success then
  begin
    ShowMessage(DataModuleMain.ApiClient.ApiResponse.Message);
    ModalResult := mrOK;
  end
  else
  begin
       DataNode := TJsonNode.Create;
       try
         DataNode.TryParse(DataModuleMain.ApiClient.ApiResponse.Data);
         UsernameError := DisplayError('name');
         EmailError := DisplayError('email');
         FirstNameError := DisplayError('first_name');
         LastNameError := DisplayError('last_name');
         PasswordError := DisplayError('password');
         PasswordConfirmationError := DisplayError('password_confirmation');
         TaxIdError := DisplayError('tax_id');
         ShowMessage('La richiesta non ha potuto essere effettuata a causa di errori. Compila tutti i campi richiesti, correggi quelli errati quindi riprova ad inviare la richiesta.');
       finally
         FreeAndNil(DataNode);
       end;

    end;
end;

procedure TFormRegisterProfile.FormCreate(Sender: TObject);
begin
  CleanErrors;
  CleanFields
end;

function TFormRegisterProfile.GetApiClient: TRTApiClient;
begin
  Result := DataModuleMain.ApiClient;
end;

function TFormRegisterProfile.GetApiResponse: TApiClientResponse;
begin
  Result := ApiClient.ApiResponse;
end;


procedure TFormRegisterProfile.CleanErrors;
begin
  EmailError := '';
  FirstNameError := '';
  LastNameError := '';
  PasswordConfirmationError := '';
  PasswordError := '';
  TaxIdError := '';
  UsernameError := '';
  Invalidate;
end;

procedure TFormRegisterProfile.CleanFields;
begin
  Email := '';
  FirstName := '';
  LastName := '';
  PasswordConfirmation := '';
  Password := '';
  TaxId := '';
  Username := '';
end;

function TFormRegisterProfile.GetEmail: String;
begin
  Result := FUserIdentifiers.Email;
end;

function TFormRegisterProfile.GetEmailError: String;
begin
  Result := FUserIdentifiers.EmailError;
end;

function TFormRegisterProfile.GetFirstName: String;
begin
  Result := FProfileData.FirstName;
end;

function TFormRegisterProfile.GetFirstNameError: String;
begin
  Result := FProfileData.FirstNameError;
end;

function TFormRegisterProfile.GetLastName: String;
begin
  Result := FProfileData.LastName;
end;

function TFormRegisterProfile.GetLastNameError: String;
begin
  Result := FProfileData.LastNameError;
end;

function TFormRegisterProfile.GetPassword: String;
begin
  Result := FSetPassword.Password;
end;

function TFormRegisterProfile.GetPasswordConfirmation: String;
begin
  Result := FSetPassword.PasswordConfirmation;
end;

function TFormRegisterProfile.GetPasswordConfirmationError: String;
begin
  Result := FSetPassword.PasswordConfirmationError;
end;

function TFormRegisterProfile.GetPasswordError: String;
begin
  Result := FSetPassword.PasswordError;
end;

function TFormRegisterProfile.GetTaxId: string;
begin
  Result := FProfileData.TaxId;
end;

function TFormRegisterProfile.GetTaxIdError: string;
begin
  Result := FProfileData.TaxIdError;
end;

function TFormRegisterProfile.GetUsername: String;
begin
  Result := FProfileData.Username;
end;

function TFormRegisterProfile.GetUsernameError: String;
begin
  Result := FProfileData.UsernameError;
end;

procedure TFormRegisterProfile.SetEmail(AValue: String);
begin
  FUserIdentifiers.Email := AValue;
end;

procedure TFormRegisterProfile.SetEmailError(AValue: String);
begin
  FUserIdentifiers.EmailError := AValue;
end;

procedure TFormRegisterProfile.SetFirstName(AValue: String);
begin
  FProfileData.FirstName := AValue;
end;

procedure TFormRegisterProfile.SetFirstNameError(AValue: String);
begin
  FProfileData.FirstNameError := AValue;

end;

procedure TFormRegisterProfile.SetLastName(AValue: String);
begin
  FProfileData.LastName := AValue;
end;

procedure TFormRegisterProfile.SetLastNameError(AValue: String);
begin
  FProfileData.LastNameError := AValue;
end;

procedure TFormRegisterProfile.SetPassword(AValue: String);
begin
  FSetPassword.Password := AValue;
end;

procedure TFormRegisterProfile.SetPasswordConfirmation(AValue: String);
begin
  FSetPassword.PasswordConfirmation := AValue;
end;

procedure TFormRegisterProfile.SetPasswordConfirmationError(AValue: String);
begin
  FSetPassword.PasswordConfirmationError := AValue;
end;

procedure TFormRegisterProfile.SetPasswordError(AValue: String);
begin
  FSetPassword.PasswordError := AValue;
end;

procedure TFormRegisterProfile.SetTaxId(AValue: string);
begin
  FProfileData.TaxId := AValue;
end;

procedure TFormRegisterProfile.SetTaxIdError(AValue: string);
begin
  FProfileData.TaxIdError := AValue;
end;

procedure TFormRegisterProfile.SetUsername(AValue: String);
begin
  FProfileData.Username := AValue;
end;

procedure TFormRegisterProfile.SetUsernameError(AValue: String);
begin
  FProfileData.UsernameError := AValue;
end;


end.

