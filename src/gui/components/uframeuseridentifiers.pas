unit UFrameUserIdentifiers;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls,
  UFrameTextFormField;

type

  { TFrameUserIdentifiers }

  TFrameUserIdentifiers = class(TFrame)
    FEmail: TTextFormField;
  private
    function GetEmail: String;
    function GetEmailError: String;
    procedure SetEmail(AValue: String);
    procedure SetEmailError(AValue: String);
    procedure SetupFields;
  protected
    procedure Loaded; override;
  public
  published
    property Email: String read GetEmail write SetEmail;
    property EmailError: String read GetEmailError write SetEmailError;
  end;

implementation

{$R *.lfm}

{ TFrameUserIdentifiers }

function TFrameUserIdentifiers.GetEmail: String;
begin
  Result := FEmail.Value;
end;

function TFrameUserIdentifiers.GetEmailError: String;
begin
  Result := FEmail.ErrorMessage;
end;


procedure TFrameUserIdentifiers.SetEmail(AValue: String);
begin
  FEmail.Value := AValue;
end;

procedure TFrameUserIdentifiers.SetEmailError(AValue: String);
begin
  FEmail.ErrorMessage := AValue;
end;


procedure TFrameUserIdentifiers.SetupFields;
begin
  with FEmail do
  begin
    FieldLabel := 'Email';
    ErrorMessage := '';
  end;
end;

procedure TFrameUserIdentifiers.Loaded;
begin
  inherited Loaded;
  SetupFields;
end;

end.

