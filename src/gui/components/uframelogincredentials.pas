unit UFrameLoginCredentials;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls,
  UFrameTextFormField, UFramePasswordField;

type

  { TFrameLoginCredentials }

  TFrameLoginCredentials = class(TFrame)
    FEmail: TTextFormField;
    FPassword: TFramePasswordField;
  private
    function GetEmail: String;
    function GetEmailError: String;
    function GetPassword: String;
    function GetPasswordError: String;
    procedure SetEmail(AValue: String);
    procedure SetEmailError(AValue: String);
    procedure SetPassword(AValue: String);
    procedure SetPasswordError(AValue: String);
    procedure SetupFields;
  protected
    procedure Loaded; override;
  public
  published
    property Email: String read GetEmail write SetEmail;
    property EmailError: String read GetEmailError write SetEmailError;
    property Password: String read GetPassword write SetPassword;
    property PasswordError: String read GetPasswordError write SetPasswordError;
  end;

implementation

{$R *.lfm}

{ TFrameLoginCredentials }

function TFrameLoginCredentials.GetEmail: String;
begin
  Result := FEmail.Value;
end;

function TFrameLoginCredentials.GetEmailError: String;
begin
  Result := FEmail.ErrorMessage;
end;

function TFrameLoginCredentials.GetPassword: String;
begin
  Result := FPassword.Value;
end;

function TFrameLoginCredentials.GetPasswordError: String;
begin
  Result := FPassword.ErrorMessage;
end;

procedure TFrameLoginCredentials.SetEmail(AValue: String);
begin
  FEmail.Value := AValue;
end;

procedure TFrameLoginCredentials.SetEmailError(AValue: String);
begin
  FEmail.ErrorMessage := AValue;
end;

procedure TFrameLoginCredentials.SetPassword(AValue: String);
begin
  FPassword.Value := AValue;
end;

procedure TFrameLoginCredentials.SetPasswordError(AValue: String);
begin
  FPassword.ErrorMessage := AValue;
end;

procedure TFrameLoginCredentials.SetupFields;
begin
  with FEmail do
  begin
    FieldLabel := 'Email';
    ErrorMessage := '';
  end;
  with FPassword do
  begin
    FieldLabel := 'Password';
    ErrorMessage := '';
    AsPassword := true;
  end;
end;

procedure TFrameLoginCredentials.Loaded;
begin
  inherited Loaded;
  SetupFields;
end;

end.

