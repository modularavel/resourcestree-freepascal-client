unit UFrameProfileData;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls,
  UFrameTextFormField;

type

  { TFrameProfileData }

  TFrameProfileData = class(TFrame)
    FFirstName: TTextFormField;
    FLastName: TTextFormField;
    FTaxId: TTextFormField;
    FUsername: TTextFormField;
  private
    function GetFirstName: String;
    function GetFirstNameError: String;
    function GetLastName: String;
    function GetLastNameError: String;
    function GetTaxId: String;
    function GetTaxIdError: String;
    function GetUsername: String;
    function GetUsernameError: String;
    procedure SetTaxId(AValue: String);
    procedure SetTaxIdError(AValue: String);
    procedure SetupFields;
    procedure SetFirstName(AValue: String);
    procedure SetFirstNameError(AValue: String);
    procedure SetLastName(AValue: String);
    procedure SetLastNameError(AValue: String);
    procedure SetUsername(AValue: String);
    procedure SetUsernameError(AValue: String);
  protected
    procedure Loaded; override;
  public
  published
    property FirstName: String read GetFirstName write SetFirstName;
    property FirstNameError: String read GetFirstNameError write SetFirstNameError;
    property LastName: String read GetLastName write SetLastName;
    property LastNameError: String read GetLastNameError write SetLastNameError;
    property TaxId: String read GetTaxId write SetTaxId;
    property TaxIdError: String read GetTaxIdError write SetTaxIdError;
    property Username: String read GetUsername write SetUsername;
    property UsernameError: String read GetUsernameError write SetUsernameError;

  end;

implementation

{$R *.lfm}

{ TFrameProfileData }

function TFrameProfileData.GetFirstName: String;
begin
  Result := FFirstName.Value;
end;

function TFrameProfileData.GetFirstNameError: String;
begin
  Result := FFirstName.ErrorMessage;
end;

function TFrameProfileData.GetLastName: String;
begin
 Result := FLastName.Value;
end;

function TFrameProfileData.GetLastNameError: String;
begin
  Result := FLastName.ErrorMessage;
end;

function TFrameProfileData.GetTaxId: String;
begin
  Result := FTaxId.Value;
end;

function TFrameProfileData.GetTaxIdError: String;
begin
  Result := FTaxId.ErrorMessage;
end;

function TFrameProfileData.GetUsername: String;
begin
  Result := FUsername.Value;
end;

function TFrameProfileData.GetUsernameError: String;
begin
  Result := FUsername.ErrorMessage;
end;

procedure TFrameProfileData.SetTaxId(AValue: String);
begin
  FTaxId.Value := AValue;
end;

procedure TFrameProfileData.SetTaxIdError(AValue: String);
begin
  FTaxId.ErrorMessage := AValue;
end;

procedure TFrameProfileData.SetupFields;
begin
  with FUserName do
  begin
    FieldLabel := 'Username (soprannome)';
    ErrorMessage := '';
  end;
  with FFirstName do
  begin
    FieldLabel := 'Nome';
    ErrorMessage := '';
  end;
  with FLastName do
  begin
    FieldLabel := 'Cognome';
    ErrorMessage := '';
  end;
  with FTaxId do
  begin
    FieldLabel := 'Codice Fiscale';
    ErrorMessage := '';
  end;
end;

procedure TFrameProfileData.SetFirstName(AValue: String);
begin
  FFirstName.Value := AValue;
end;

procedure TFrameProfileData.SetFirstNameError(AValue: String);
begin
  FFirstName.ErrorMessage := AValue;
end;

procedure TFrameProfileData.SetLastName(AValue: String);
begin
  FLastName.Value := AValue;
end;

procedure TFrameProfileData.SetLastNameError(AValue: String);
begin
  FLastName.ErrorMessage := AValue;
end;

procedure TFrameProfileData.SetUsername(AValue: String);
begin
  FUsername.Value := AValue;
end;

procedure TFrameProfileData.SetUsernameError(AValue: String);
begin
  FUsername.ErrorMessage := AValue;
end;

procedure TFrameProfileData.Loaded;
begin
  inherited Loaded;
  SetupFields;
end;

end.

