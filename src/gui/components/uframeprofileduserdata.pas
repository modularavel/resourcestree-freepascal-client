unit UFrameProfiledUserData;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, ExtCtrls, StdCtrls;

type

  { TFrameProfiledUserData }

  TFrameProfiledUserData = class(TFrame)
    ValueTaxId: TLabel;
    ValueName: TLabel;
    ValueCredentials: TLabel;
    Panel1: TPanel;
  private
    FEmail: String;
    FFirstName: String;
    FLastName: String;
    FUsername: String;
    FTaxId: String;

    procedure SetEmail(AValue: String);
    procedure SetFirstName(AValue: String);
    procedure SetLastName(AValue: String);
    procedure SetTaxId(AValue: String);
    procedure SetUsername(AValue: string);
  public
  published
    property Email: String read FEmail write SetEmail;
    property FirstName: String read FFirstName write SetFirstName;
    property LastName: String read FLastName write SetLastName;
    property Username: String read FUsername write SetUsername;
    property TaxId: String read FTaxId write SetTaxId;

  end;

implementation

{$R *.lfm}

{ TFrameProfiledUserData }


procedure TFrameProfiledUserData.SetEmail(AValue: String);
begin
  FEmail := AValue;
  ValueCredentials.Caption := format('%s (%s)', [FEmail, FUsername]);
end;

procedure TFrameProfiledUserData.SetFirstName(AValue: String);
begin
  FFirstName := AValue;;
  ValueName.Caption := format('%s %s', [FFirstName, FLastName]);
end;

procedure TFrameProfiledUserData.SetLastName(AValue: String);
begin
  FLastName := AValue;;
  ValueName.Caption := format('%s %s', [FFirstName, FLastName]);
end;

procedure TFrameProfiledUserData.SetTaxId(AValue: String);
begin
  FTaxId := AValue;
  ValueTaxId.Caption := FTaxId;
end;

procedure TFrameProfiledUserData.SetUsername(AValue: string);
begin
  FUsername := AValue;
  ValueCredentials.Caption := format('%s (%s)', [FEmail, FUsername]);
end;

end.

