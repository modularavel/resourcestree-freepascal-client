unit UFramePasswordField;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, ExtCtrls, StdCtrls,
  UFrameTextFormField;

type

  { TFramePasswordField }

  TFramePasswordField = class(TFrame)
    FShowPassword: TCheckBox;
    PanelPasswordField: TPanel;
    Splitter1: TSplitter;
    FPasswordField: TTextFormField;
    procedure FShowPasswordChange(Sender: TObject);
  private
    function GetAsPassword: Boolean;
    function GetErrorMessage: String;
    function GetFieldLabel: String;
    function GetValue: String;
    procedure SetAsPassword(AValue: Boolean);
    procedure SetErrorMessage(AValue: String);
    procedure SetFieldLabel(AValue: String);
    procedure SetValue(AValue: String);
    procedure SetupFields;
  protected
    procedure Loaded; override;
  published
    property AsPassword: Boolean read GetAsPassword write SetAsPassword;
    property ErrorMessage: String read GetErrorMessage write SetErrorMessage;
    property FieldLabel: String read GetFieldLabel write SetFieldLabel;
    property Value: String read GetValue write SetValue;
  end;

implementation

{$R *.lfm}

{ TFramePasswordField }

procedure TFramePasswordField.FShowPasswordChange(Sender: TObject);
begin
  FPasswordField.AsPassword := not FShowPassword.Checked;
end;

function TFramePasswordField.GetAsPassword: Boolean;
begin
  Result := FPasswordField.AsPassword;
end;

function TFramePasswordField.GetErrorMessage: String;
begin
  Result := FPasswordField.ErrorMessage;
end;

function TFramePasswordField.GetFieldLabel: String;
begin
  Result := FPasswordField.FieldLabel;
end;

function TFramePasswordField.GetValue: String;
begin
  Result := FPasswordField.Value;
end;

procedure TFramePasswordField.SetAsPassword(AValue: Boolean);
begin
  FPasswordField.AsPassword := AsPassword;
end;

procedure TFramePasswordField.SetErrorMessage(AValue: String);
begin
  FPasswordField.ErrorMessage := AValue;
end;

procedure TFramePasswordField.SetFieldLabel(AValue: String);
begin
  FPasswordField.FieldLabel := AValue;
end;

procedure TFramePasswordField.SetValue(AValue: String);
begin
  FPasswordField.Value := AValue;
end;

procedure TFramePasswordField.SetupFields;
begin
  FShowPassword.Checked := False;
  FPasswordField.AsPassword := True;
end;

procedure TFramePasswordField.Loaded;
begin
  inherited Loaded;
  SetupFields;
end;

end.

