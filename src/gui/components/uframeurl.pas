unit UFrameUrl;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, ExtCtrls, StdCtrls;

type

  { TFrameUrl }

  TFrameUrl = class(TFrame)
    FEditPath: TEdit;
    FEditPort: TEdit;
    FEditHost: TEdit;
    FEditProtocol: TComboBox;
    FFieldError: TLabel;
    FFieldPanel: TPanel;
    FFieldLabel: TLabel;
    FieldPanel: TPanel;
    FLabelHostSeparator: TEdit;
    FLabelPathSeparator: TEdit;
    FLabelPortSeparator: TEdit;
  private
    function GetErrorMessage: String;
    function GetFieldLabel: string;
    function GetHost: String;
    function GetPath: String;
    function GetPort: String;
    function GetProtocol: String;
    function GetValue: string;
    procedure SetErrorMessage(AValue: String);
    procedure SetFieldLabel(AValue: string);
    procedure SetHost(AValue: String);
    procedure SetPath(AValue: String);
    procedure SetPort(AValue: String);
    procedure SetProtocol(AValue: String);
    procedure SetValue(AValue: string);
    function StripSlashes(Source: String): String;
  public
  published
    property ErrorMessage: String read GetErrorMessage write SetErrorMessage;
    property FieldLabel: string read GetFieldLabel write SetFieldLabel;
    property Host: String read GetHost write SetHost;
    property Path: String read GetPath write SetPath;
    property Port: String read GetPort write SetPort;
    property Protocol: String read GetProtocol write SetProtocol;
    property Value: string read GetValue write SetValue;



  end;

implementation

uses
  StrUtils;

{$R *.lfm}

{ TFrameUrl }

function TFrameUrl.GetErrorMessage: String;
begin
  Result := FFieldError.Caption;
end;

function TFrameUrl.GetFieldLabel: string;
begin
  Result := FFieldLabel.Caption;
end;

function TFrameUrl.GetHost: String;
begin
  Result := FEditHost.Text;
end;

function TFrameUrl.GetPath: String;
begin
  Result := FEditPath.Text;
end;

function TFrameUrl.GetPort: String;
begin
  Result := FEditPort.Text;
end;

function TFrameUrl.GetProtocol: String;
begin
  Result := FEditProtocol.Text;
end;

function TFrameUrl.GetValue: string;
begin
  Result := Format('%s://%s:%s/%s/', [Protocol, Host, Port, Path]);
end;

procedure TFrameUrl.SetErrorMessage(AValue: String);
begin
  FFieldError.Caption := AValue;
end;

procedure TFrameUrl.SetFieldLabel(AValue: string);
begin
  FFieldLabel.Caption := AValue;
end;

procedure TFrameUrl.SetHost(AValue: String);
begin
  FEditHost.Text := AValue;
end;

procedure TFrameUrl.SetPath(AValue: String);
begin
  FEditPath.Text := StripSlashes(AValue);
end;

procedure TFrameUrl.SetPort(AValue: String);
begin
  FEditPort.Text := AValue;
end;

procedure TFrameUrl.SetProtocol(AValue: String);
begin
//  FEditProtocol.Text := AValue;
  FEditProtocol.ItemIndex := FEditProtocol.Items.IndexOf(AValue);

end;

procedure TFrameUrl.SetValue(AValue: string);
begin

end;


function TFrameUrl.StripSlashes(Source: String): String;
begin
  RemoveTrailingChars(Source, ['/']);
  Removeleadingchars(Source, ['/']);
  Result := Source;
end;

end.

