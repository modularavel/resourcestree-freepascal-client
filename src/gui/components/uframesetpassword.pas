unit UFrameSetPassword;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, ExtCtrls,
  UFramePasswordField;

type

  { TFrameSetPassword }

  TFrameSetPassword = class(TFrame)
    FPassword: TFramePasswordField;
    FPasswordConfirmation: TFramePasswordField;
    PanelFields: TPanel;
  private
    function GetPassword: String;
    function GetPasswordConfirmation: String;
    function GetPasswordConfirmationError: String;
    function GetPasswordError: String;
    procedure SetPassword(AValue: String);
    procedure SetPasswordConfirmation(AValue: String);
    procedure SetPasswordConfirmationError(AValue: String);
    procedure SetPasswordError(AValue: String);
    procedure SetupFields;
  protected
    procedure Loaded; override;
  public
    property Password: String read GetPassword write SetPassword;
    property PasswordError: String read GetPasswordError write SetPasswordError;
    property PasswordConfirmation: String read GetPasswordConfirmation write SetPasswordConfirmation;
    property PasswordConfirmationError: String read GetPasswordConfirmationError write SetPasswordConfirmationError;

  end;

implementation

{$R *.lfm}

{ TFrameSetPassword }

function TFrameSetPassword.GetPassword: String;
begin
  Result := FPassword.Value;
end;

function TFrameSetPassword.GetPasswordConfirmation: String;
begin
  Result := FPasswordConfirmation.Value;
end;

function TFrameSetPassword.GetPasswordConfirmationError: String;
begin
  Result := FPasswordConfirmation.ErrorMessage;
end;

function TFrameSetPassword.GetPasswordError: String;
begin
  Result := FPassword.ErrorMessage;
end;

procedure TFrameSetPassword.SetPassword(AValue: String);
begin
  FPassword.Value := AValue;
end;

procedure TFrameSetPassword.SetPasswordConfirmation(AValue: String);
begin
  FPasswordConfirmation.Value := AValue;
end;

procedure TFrameSetPassword.SetPasswordConfirmationError(AValue: String);
begin
  FPasswordConfirmation.ErrorMessage := AValue;
end;

procedure TFrameSetPassword.SetPasswordError(AValue: String);
begin
  FPassword.ErrorMessage := AValue;
end;

procedure TFrameSetPassword.SetupFields;
begin
    with FPassword do
    begin
      FieldLabel := 'Password';
      ErrorMessage := '';
      AsPassword := true;
    end;
    with FPasswordConfirmation do
    begin
      FieldLabel := 'Conferma password';
      ErrorMessage := '';
      AsPassword := true;
    end;
end;

procedure TFrameSetPassword.Loaded;
begin
  inherited Loaded;
  SetupFields;
end;

end.

