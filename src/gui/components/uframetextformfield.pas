unit UFrameTextFormField;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, ExtCtrls, StdCtrls;

type

  { TTextFormField }

  TTextFormField = class(TFrame)
    FFieldEdit: TEdit;
    FFieldLabel: TLabel;
    FFieldError: TLabel;
    FFieldPanel: TPanel;
  private
    function GetAsPassword: Boolean;
    function GetErrorMessage: String;
    function GetFieldLabel: string;
    function GetValue: string;
    procedure SetAsPassword(AValue: Boolean);
    procedure SetErrorMessage(AValue: String);
    procedure SetFieldLabel(AValue: string);
    procedure SetValue(AValue: string);
  public
  published
    property AsPassword: Boolean read GetAsPassword write SetAsPassword;
    property ErrorMessage: String read GetErrorMessage write SetErrorMessage;
    property FieldLabel: string read GetFieldLabel write SetFieldLabel;
    property Value: string read GetValue write SetValue;
  end;

implementation

{$R *.lfm}

{ TTextFormField }

function TTextFormField.GetAsPassword: Boolean;
begin
  Result := FFieldEdit.EchoMode = emPassword;
end;

function TTextFormField.GetErrorMessage: String;
begin
  Result := FFieldError.Caption;
end;

function TTextFormField.GetFieldLabel: string;
begin
  Result := FFieldLabel.Caption;
end;

function TTextFormField.GetValue: string;
begin
  Result := FFieldEdit.Text;
end;

procedure TTextFormField.SetAsPassword(AValue: Boolean);
begin
  if AValue then
  begin
    FFieldEdit.EchoMode := emPassword;
  end
  else
  begin
    FFieldEdit.EchoMode := emNormal;
  end;
end;

procedure TTextFormField.SetErrorMessage(AValue: String);
begin
  FFieldError.Caption := AValue;
end;

procedure TTextFormField.SetFieldLabel(AValue: string);
begin
  FFieldLabel.Caption := AValue;
end;

procedure TTextFormField.SetValue(AValue: string);
begin
  FFieldEdit.Text := AValue;
end;

end.

