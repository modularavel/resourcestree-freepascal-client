unit UUpdateProfile;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls,
  Buttons,
  UFrameProfileData,
  ApiClientResponse, ResourcesTreeApiClient;

type

  { TFormUpdateProfile }

  TFormUpdateProfile = class(TForm)
    BtnCancel: TBitBtn;
    BtnOK: TBitBtn;
    FProfileData: TFrameProfileData;
    PanelButtons: TPanel;
    procedure BtnOKClick(Sender: TObject);
  private
    procedure CleanForm;
    function GetApiClient: TRTApiClient;
    function GetApiResponse: TApiClientResponse;
    function GetFirstName: String;
    function GetFirstNameError: String;
    function GetLastName: String;
    function GetLastNameError: String;
    function GetTaxId: string;
    function GetTaxIdError: string;
    function GetUsername: String;
    function GetUsernameError: String;
    procedure SetFirstName(AValue: String);
    procedure SetFirstNameError(AValue: String);
    procedure SetLastName(AValue: String);
    procedure SetLastNameError(AValue: String);
    procedure SetTaxId(AValue: string);
    procedure SetTaxIdError(AValue: string);
    procedure SetUsername(AValue: String);
    procedure SetUsernameError(AValue: String);
  protected
    property ApiClient: TRTApiClient read GetApiClient;
    property ApiResponse: TApiClientResponse read GetApiResponse;
  public
  published
    property FirstName: String read GetFirstName write SetFirstName;
    property FirstNameError: String read GetFirstNameError write SetFirstNameError;
    property LastName: String read GetLastName write SetLastName;
    property LastNameError: String read GetLastNameError write SetLastNameError;
    property TaxId: string read GetTaxId write SetTaxId;
    property TaxIdError: string read GetTaxIdError write SetTaxIdError;
    property Username: String read GetUsername write SetUsername;
    property UsernameError: String read GetUsernameError write SetUsernameError;
  end;

var
  FormUpdateProfile: TFormUpdateProfile;

implementation
uses DMMain, JsonTools;
{$R *.lfm}

{ TFormUpdateProfile }

procedure TFormUpdateProfile.BtnOKClick(Sender: TObject);
var
  Result : Boolean;
  DataNode: TJsonNode;
  ErrorFieldNode: TJsonNode;

  function DisplayError(FieldName: string): String;
    begin
      ErrorFieldNode := DataNode.find(FieldName);
      if ErrorFieldNode <> nil then
      begin
        Result := StripQuotes(ErrorFieldNode.Child(0).Value);
      end
      else
      begin
        Result := '';
      end;
    end;

begin
  CleanForm;
  Result := DataModuleMain.UpdateProfile(
    DataModuleMain.UserId,
    FProfileData.Username,
    FProfileData.FirstName,
    FProfileData.LastName,
    FProfileData.TaxId
  );

  if DataModuleMain.ApiClient.ApiResponse.Success then
  begin
    ShowMessage(DataModuleMain.ApiClient.ApiResponse.Message);
    ModalResult := mrOK;
  end
  else
  begin
    if ApiResponse.BadRequest then
    begin
      DataNode := TJsonNode.Create;
       try
         DataNode.TryParse(DataModuleMain.ApiClient.ApiResponse.Data);
         UsernameError := DisplayError('name');
         FirstNameError := DisplayError('first_name');
         LastNameError := DisplayError('last_name');
         TaxIdError := DisplayError('tax_id');

         ShowMessage('La richiesta non ha potuto essere effettuata a causa di errori. Compila tutti i campi richiesti, correggi quelli errati quindi riprova ad inviare la richiesta.');
       finally
         FreeAndNil(DataNode);
       end;
    end
    else
    begin
      ShowMessage('Errore interno del server: ' + DataModuleMain.ApiClient.ApiResponse.Message);
    end;
  end;
end;

procedure TFormUpdateProfile.CleanForm;
begin
  FirstNameError := '';
  LastNameError := '';
  TaxIdError := '';
end;

function TFormUpdateProfile.GetApiClient: TRTApiClient;
begin
  Result := DataModuleMain.ApiClient;
end;

function TFormUpdateProfile.GetApiResponse: TApiClientResponse;
begin
  Result := ApiClient.ApiResponse;
end;

function TFormUpdateProfile.GetFirstName: String;
begin
  Result := FProfileData.FirstName;
end;

function TFormUpdateProfile.GetFirstNameError: String;
begin
  Result := FProfileData.FirstNameError;
end;

function TFormUpdateProfile.GetLastName: String;
begin
  Result := FProfileData.LastName;
end;

function TFormUpdateProfile.GetLastNameError: String;
begin
  Result := FProfileData.LastNameError;
end;

function TFormUpdateProfile.GetTaxId: string;
begin
  Result := FProfileData.TaxId;
end;

function TFormUpdateProfile.GetTaxIdError: string;
begin
  Result := FProfileData.TaxIdError;
end;

function TFormUpdateProfile.GetUsername: String;
begin
  Result := FProfileData.Username;
end;

function TFormUpdateProfile.GetUsernameError: String;
begin
  Result := FProfileData.UsernameError;
end;

procedure TFormUpdateProfile.SetFirstName(AValue: String);
begin
  FProfileData.FirstName := AValue;
end;

procedure TFormUpdateProfile.SetFirstNameError(AValue: String);
begin
  FProfileData.FirstNameError := AValue;
end;

procedure TFormUpdateProfile.SetLastName(AValue: String);
begin
  FProfileData.LastName := AValue;
end;

procedure TFormUpdateProfile.SetLastNameError(AValue: String);
begin
  FProfileData.LastNameError := AValue;
end;

procedure TFormUpdateProfile.SetTaxId(AValue: string);
begin
  FProfileData.TaxId := AValue;
end;

procedure TFormUpdateProfile.SetTaxIdError(AValue: string);
begin
  FProfileData.TaxIdError := AValue;
end;

procedure TFormUpdateProfile.SetUsername(AValue: String);
begin
  FProfileData.Username := AValue;
end;

procedure TFormUpdateProfile.SetUsernameError(AValue: String);
begin
  FProfileData.UsernameError := AValue;
end;

end.

