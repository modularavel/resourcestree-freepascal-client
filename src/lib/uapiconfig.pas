unit UApiConfig;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, XMLConf, URIParser;

type

  { TApiConfig }

  TApiConfig = class
  private
    FUri: TURI;
    function GetApplicationConfig: TXmlConfig;
    function GetBasePath: String;
    function GetBaseUrl: string;
    function GetHost: string;
    function GetPassword: String;
    function GetPath: String;
    function GetPort: Word;
    function GetProtocol: String;
    function GetUri: String;
    function GetUsername: String;
    procedure SetBasePath(AValue: String);
    procedure SetBaseUrl(AValue: string);
    procedure SetHost(AValue: string);
    procedure SetPassword(AValue: String);
    procedure SetPath(AValue: String);
    procedure SetPort(AValue: Word);
    procedure SetProtocol(AValue: String);
    procedure SetUri(AValue: String);
    procedure SetUsername(AValue: String);
    function StripSlashes(Source: String): String;
  protected
    property ApplicationConfig: TXmlConfig read GetApplicationConfig;
  public
    procedure LoadConfigValues;
    procedure SaveConfigValues;
  published
    property BasePath: String read GetBasePath write SetBasePath;
    property BaseUrl: string read GetBaseUrl write SetBaseUrl;
    property Host: string read GetHost write SetHost;
    property Path: String read GetPath write SetPath;
    property Port: Word read GetPort write SetPort;
    property Protocol: String read GetProtocol write SetProtocol;
    property Password: String read GetPassword write SetPassword;
    property Uri: String read GetUri write SetUri;
    property Username: String read GetUsername write SetUsername;

  end;

implementation

uses
  StrUtils,
  DmMain;

const
  CONFIGPATH_BASEURL = 'api/baseurl';
  CONFIGPATH_USERNAME = 'api/auth/username';
  CONFIGPATH_PASSWORD = 'api/auth/password';


{ TApiConfig }

function TApiConfig.GetBasePath: String;
begin
  Result := FUri.Path;
end;

function TApiConfig.GetBaseUrl: string;
begin
  with FUri do
  begin
    Result := format('%s://%s:%d/%s/',[
      Protocol,
      Host,
      Port,
      StripSlashes(Path)
    ]);
  end;
end;

function TApiConfig.GetApplicationConfig: TXmlConfig;
begin
  Result := DataModuleMain.ApplicationConfig;
end;

function TApiConfig.GetHost: string;
begin
  Result := FUri.Host;
end;

function TApiConfig.GetPassword: String;
begin
  Result := FUri.Password;
end;

function TApiConfig.GetPath: String;
begin
  Result := FUri.Path;
end;

function TApiConfig.GetPort: Word;
begin
  Result := FUri.Port;
end;

function TApiConfig.GetProtocol: String;
begin
  Result := FUri.Protocol;
end;

function TApiConfig.GetUri: String;
begin
  Result := EncodeURI(FUri);
end;

function TApiConfig.GetUsername: String;
begin
  Result := FUri.Username;
end;


procedure TApiConfig.SetBasePath(AValue: String);
begin
  FUri.Path := AValue;
end;

procedure TApiConfig.SetBaseUrl(AValue: string);
begin
  FUri := ParseUri(AValue);
end;

procedure TApiConfig.SetHost(AValue: string);
begin
  FUri.Host := AValue;
end;

procedure TApiConfig.SetPassword(AValue: String);
begin
  FUri.Password := AValue;
end;

procedure TApiConfig.SetPath(AValue: String);
begin
  if AValue[1] <> '/' then
  begin
    AValue := '/' + AValue;
  end;
  FUri.Path := AValue;
end;

procedure TApiConfig.SetPort(AValue: Word);
begin
  FUri.Port := AValue;
end;

procedure TApiConfig.SetProtocol(AValue: String);
begin
  FUri.Protocol := AValue;
end;

procedure TApiConfig.SetUri(AValue: String);
begin
  FUri := ParseURI(AValue);
end;

procedure TApiConfig.SetUsername(AValue: String);
begin
  FUri.Username := AValue;
end;

function TApiConfig.StripSlashes(Source: String): String;
begin
  RemoveTrailingChars(Source, ['/']);
  Removeleadingchars(Source, ['/']);
  Result := Source;
end;

procedure TApiConfig.LoadConfigValues;
begin
  {
    Avoid using native EncodeURI because of possible conflicts due to special
    chars (as '@','/',':') in username or password that will break persing so
    username and password are stored in dedicated fields of the config file.
  }
  BaseUrl := ApplicationConfig.GetValue(CONFIGPATH_BASEURL,'http://localhost:8000/api/v1/');
  Username := ApplicationConfig.GetValue(CONFIGPATH_USERNAME,'');
  Password := ApplicationConfig.GetValue(CONFIGPATH_PASSWORD,'');
end;

procedure TApiConfig.SaveConfigValues;
begin
  ApplicationConfig.SetValue(CONFIGPATH_BASEURL,BaseUrl);
  ApplicationConfig.SetValue(CONFIGPATH_USERNAME,Username);
  ApplicationConfig.SetValue(CONFIGPATH_PASSWORD,Password);
  ApplicationConfig.Flush;
end;


end.

