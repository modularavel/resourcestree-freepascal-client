unit ApiClientResponse;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils,
  JsonTools, RESTRequest4D;

type

  { TApiClientStatus }

  { TApiClientResponse }

  TApiClientResponse = class
  private
    FStatus: Single;
    FMessage: String;
    FData: String;
    FHasPagination: Boolean;

    function GetBadRequest: Boolean;
    function GetInternalError: Boolean;
    function GetSuccess: Boolean;
    procedure SetRequestResponse(aRequestResponse: IResponse);
  public
    constructor Create(aRequestResponse: IResponse); overload;
    constructor Create(aStatus: Single; aMessage: String); overload;
    destructor Destroy; override;
  published
    property Data: String read FData;
    property BadRequest: Boolean read GetBadRequest;
    property HasPagination: Boolean read FHasPagination;
    property InternalError: Boolean read GetInternalError;
    property Message: string read FMessage;
    property RequestResponse: IResponse write SetRequestResponse;
    property Status:single read FStatus;
    property Success: Boolean read GetSuccess;
  end;



implementation

uses Variants;

const
  KEY_METADATA = 'metadata';
  KEY_DATA = 'data';
  KEY_MESSAGE = 'message';

{ TApiClientResponse }



function TApiClientResponse.GetSuccess: Boolean;
begin
  Result := VarInRange(FStatus, 200, 299);
end;

function TApiClientResponse.GetInternalError: Boolean;
begin
  Result := VarInRange(FStatus, 500, 599);
end;

function TApiClientResponse.GetBadRequest: Boolean;
begin
    Result := VarInRange(FStatus, 400, 499);
end;

procedure TApiClientResponse.SetRequestResponse(aRequestResponse: IResponse);
var
  ResponseNode: TJsonNode;
  TempJsonString,y: string;
  aa: string;
begin
  FStatus := aRequestResponse.StatusCode.ToSingle;
  ResponseNode := TJsonNode.Create;

  try
    ResponseNode.TryParse(aRequestResponse.Content);
    FMessage := ResponseNode.Find('metadata/message').AsString;

    TempJsonString := ResponseNode.Find('data').Value;

    TempJsonString := Copy(TempJsonString, Pos('{', TempJsonString), Length(TempJsonString));
    FData := TempJsonString;

  finally
    if Assigned(ResponseNode) then
    begin
      FreeAndNil(ResponseNode);
    end;
  end;
end;


constructor TApiClientResponse.Create(aRequestResponse: IResponse);
begin
  SetRequestResponse(aRequestResponse);
end;

constructor TApiClientResponse.Create(aStatus: Single; aMessage: String);
begin
  FStatus := aStatus;
  FMessage := aMessage;
  FData := '{ "data": {} }';
end;

destructor TApiClientResponse.Destroy;
begin
  inherited Destroy;
end;

{ TApiClientStatus }



end.

