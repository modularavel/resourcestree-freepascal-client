unit ResourcesTreeApiClient;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils,
  RESTRequest4D, ApiClientResponse, UApiConfig;

const
  ConfigHostPath = 'api/host';
  ConfigPortPath = 'api/port';
  ConfigProtocolPath = 'api/protocol';
  ConfigBasePath = 'api/basePath';

  ConfigEmailPath = 'auth/email';
  ConfigPasswordPath = 'auth/password';
  ConfigSavePasswordPath = 'auth/save-password';

type

  { TRTApiClient }

  TRTApiClient = class
    private
      FToken: String;
      FApiResponse: TApiClientResponse;
      FParameters: TStringList;
      FRequestResponse: IResponse;
      FResponseString: String;

      function GetApiConfig: TApiConfig;
      function GetDataValue(Key: String): String;
      function GetLoggedIn: Boolean;

      function LoginRequestBody(aEmail: String; aPassword: String): String;
      function PatchRequest(Path: String; aId: Integer; Body: String): single;
      function PostRequest(Path: String; Authenticated: Boolean): single; overload;
      function PostRequest(Path: String; Body: String; Authenticated: Boolean): single; overload;
      function RegisterProfileBody(aEmail: String; aPassword: String; aPasswordConfirmation: String;
               aUsername: String; aFirstName: String; aLastName: String; aTaxId: String): String;
      function StripQuotes(Source: String): String;
      function UpdateProfileBody(aUsername: String; aFirstName: String; aLastName: String; aTaxId: String): String;

    public
      constructor Create;
      destructor Destroy; override;
      function Login(aEmail: String; aPassword: String): Boolean;
      function Logout: Boolean;
      function RegisterProfile(aEmail: String; aPassword: String; aPasswordConfirmation: String;
               aUsername: String; aFirstName: String; aLastName: String; aTaxId: String): Boolean;
      function UpdateProfile(aUserId: integer; aUsername: String; aFirstName: String; aLastName: String; aTaxId: String): Boolean;

      property DataValue[Key: String] : String read GetDataValue;

    published
      property ApiResponse: TApiClientResponse read FApiResponse;
      property ApiConfig: TApiConfig read GetApiConfig;
      property LoggedIn: Boolean read GetLoggedIn;

  end;


implementation

uses DMMain, JsonTools, Variants, Dialogs;

const

  DefaultMimeType = 'application/json';

  LoginPath = 'auth/login';
  LogoutPath = 'auth/logout';
  RegisterPath = 'auth/register';
  ChangePasswordPath = 'auth/password';
  ForgotPasswordPath = 'auth/forgot-password';

  UpdatePath = 'profile';


{ TRTApiClient }

function TRTApiClient.GetLoggedIn: Boolean;
begin
  Result := FToken <> '';
end;



function TRTApiClient.GetDataValue(Key: String): String;
var
  JsonNode: TJsonNode;
begin
  Result := '';
  JSonNode := TJsonNode.Create;
  try
    JsonNode.Parse(ApiResponse.Data);


    if JsonNode.Exists(Key) then
    begin
      case JsonNode.Find(Key).Kind of
           nkNumber : Result := JsonNode.Find(Key).Value;
           nkString : Result := StripQuotes(JsonNode.Find(Key).Value);
           nkArray : Result := JsonNode.Find(Key).Value;
      end;
    end;
  finally
    FreeAndNil(JsonNode);
  end;
end;

function TRTApiClient.GetApiConfig: TApiConfig;
begin
  Result := DataModuleMain.ApiConfig;
end;


function TRTApiClient.LoginRequestBody(aEmail: String; aPassword: String
  ): String;
var
  RequestJson: TJsonNode;
begin
  try
    RequestJson := TJsonNode.Create;
    RequestJson.Force('email').AsString :=aEmail;
    RequestJson.Force('password').AsString := aPassword;
    Result := RequestJson.Value;
  finally
    FreeAndNil(RequestJSon);
  end;
end;

function TRTApiClient.PatchRequest(Path: String; aId: Integer; Body: String
  ): single;
var
  Request: IRequest;
  Uri: string;
begin
  FResponseString := '';
  Uri := Format('%s%s/%d',[ApiConfig.BaseUrl, Path, aId]);

  Request := TRequest.New.BaseURL(Uri)
        .Accept(DefaultMimeType)
        .ContentType(DefaultMimeType)
        .TokenBearer(FToken)
        .AddBody(Body);
  try
    FRequestResponse := Request.Patch;
  except
    on E: Exception do
    begin
      ShowMessage(format('Errore nella chiamata API:' + #13#13#13#10 + '"%s"', [E.Message]));
      FApiResponse := TApiClientResponse.Create(500, E.Message);
      Exit(500);
    end;
  end;

  FApiResponse := TApiClientResponse.Create(FRequestResponse);
  Result := FApiResponse.Status;
end;

function TRTApiClient.PostRequest(Path: String; Authenticated: Boolean): single;
begin
  Result := PostRequest(Path, '', Authenticated);
end;

function TRTApiClient.PostRequest(Path: String; Body: String; Authenticated: Boolean): single;
var
  Request: IRequest;
  Uri: string;
begin
  FResponseString := '';
  Uri := ApiConfig.BaseUrl + Path;
  Request := TRequest.New.BaseURL(Uri).Accept(DefaultMimeType);

  if Body <> '' then
  begin
    Request.ContentType(DefaultMimeType).AddBody(Body)
  end;

  if Authenticated then
  begin
    Request.TokenBearer(FToken);
  end;

  try
    FRequestResponse := Request.Post;
  except
    on E: Exception do
    begin
      ShowMessage(format('Errore nella chiamata API: + #13#13#13#10 + "%s"', [E.Message]));
      FApiResponse := TApiClientResponse.Create(500, E.Message);
      Exit(500);
    end;
  end;

  FApiResponse := TApiClientResponse.Create(FRequestResponse);
  Result := FApiResponse.Status;
end;

function TRTApiClient.RegisterProfileBody(
         aEmail: String;
         aPassword: String;
         aPasswordConfirmation: String;
         aUsername: String;
         aFirstName: String;
         aLastName: String;
         aTaxId: String): String;
var
  RequestJson: TJsonNode;
begin
  try
      RequestJson := TJsonNode.Create;
      RequestJson.Force('email').AsString := aEmail;
      RequestJson.Force('name').AsString := aUsername;
      RequestJson.Force('first_name').AsString := aFirstName;
      RequestJson.Force('last_name').AsString := aLastName;
      RequestJson.Force('password').AsString := aPassword;
      RequestJson.Force('password_confirmation').AsString := aPasswordConfirmation;

      if aTaxId <> '' then
      begin
        RequestJson.Force('tax_id').AsString := aTaxId;
      end;
      Result := RequestJson.Value;

    finally
      FreeAndNil(RequestJson);
    end;
end;


function TRTApiClient.StripQuotes(Source: String): String;
begin
  Result := Copy(Source,2,Length(Source)-2);
end;

function TRTApiClient.UpdateProfileBody(aUsername: String; aFirstName: String; aLastName: String;
  aTaxId: String): String;
var
  RequestJson: TJsonNode;
begin
  try
      RequestJson := TJsonNode.Create;
      RequestJson.Force('name').AsString := aUsername;
      RequestJson.Force('first_name').AsString := aFirstName;
      RequestJson.Force('last_name').AsString := aLastName;

      if aTaxId <> '' then
      begin
        RequestJson.Force('tax_id').AsString := aTaxId;
      end;
      Result := RequestJson.Value;

    finally
      FreeAndNil(RequestJson);
    end;
end;

constructor TRTApiClient.Create;
begin
  FToken := '';
  FParameters := TStringList.Create;
end;

destructor TRTApiClient.Destroy;
begin
  if Assigned(FApiResponse) then
  begin
    FreeAndNil(FApiResponse);
  end;
  if Assigned(FParameters) then
  begin
    FreeAndNil(FParameters);
  end;
  inherited Destroy;
end;

function TRTApiClient.Login(aEmail: String; aPassword: String): Boolean;
var
  DataNode: TJsonNode;
begin
  PostRequest(LoginPath, LoginRequestBody(aEmail, aPassword), False);
  Result := FApiResponse.Success;
  if FApiResponse.Success then
  try
    DataNode := TJsonNode.Create;
    DataNode.Parse(FApiResponse.Data);

    FToken := DataNode.Find('auth_token').AsString;

  finally
    FreeAndNil(DataNode);
  end
  else
  begin
    FToken := '';
  end;
end;

function TRTApiClient.Logout: Boolean;
begin
  PostRequest(LogoutPath, True);
  Result := FApiResponse.Success;
  if Result then
  begin
    FToken := '';
  end;
end;

function TRTApiClient.RegisterProfile(
  aEmail: String;
  aPassword: String;
  aPasswordConfirmation: String;
  aUsername: String;
  aFirstName: String;
  aLastName: String;
  aTaxId: String): Boolean;
var
  Body: String;
begin
  Body := RegisterProfileBody(
    aEmail, aPassword,
    aPasswordConfirmation,
    aUsername,
    aFirstName,
    aLastName,
    aTaxId);
  PostRequest(RegisterPath,Body, False);
  Result := FApiResponse.Success;
end;

function TRTApiClient.UpdateProfile(
  aUserId: integer;
  aUsername: String;
  aFirstName: String;
  aLastName: String;
  aTaxId: String): Boolean;
var
  Body: String;
begin
  Body := UpdateProfileBody(aUsername, aFirstName, aLastName, aTaxId);
  PatchRequest(UpdatePath, aUserId, Body);
  Result := FApiResponse.Success;
end;

end.

