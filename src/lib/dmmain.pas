unit DMMain;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Controls, ActnList, XMLConf,
  ResourcesTreeApiClient, UApiConfig;

type

  { TDataModuleMain }

  TDataModuleMain = class(TDataModule)
    ActionRegisterProfile: TAction;
    ActionShowLoginForm: TAction;
    MainActionList: TActionList;
    FApplicationConfig: TXMLConfig;
    MainImageList: TImageList;
    procedure ActionRegisterProfileExecute(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    FApiClient: TRTApiClient;
    FApiConfig: TApiConfig;
    FUserId: Integer;
    function GetLoggedIn: Boolean;
    function GetSavedEmail: String;
    function GetSavedPassword: string;
    procedure SetSavedEmail(AValue: String);
    procedure SetSavedPassword(AValue: string);
  public
    function Login(aEmail: String; aPassword: String): Boolean;
    function RegisterProfile(aEmail: String; aPassword: String; aPasswordConfirmation: String;
               aUsername: String; aFirstName: String; aLastName: String; aTaxId: String): Boolean;
    procedure SaveApiConfig;
    function UpdateProfile(aUserId: integer; aUsername: String; aFirstName: String; aLastName: String; aTaxId: String): Boolean;

  published
    property ApiConfig :TApiConfig read FApiConfig;
    property ApplicationConfig: TXMLConfig read FApplicationConfig;
    property LoggedIn: Boolean read GetLoggedIn;
    property ApiClient: TRTApiClient read FApiClient;
    property SavedEmail: String read GetSavedEmail write SetSavedEmail;
    property SavedPassword: string read GetSavedPassword write SetSavedPassword;
    property UserId: integer read FUserId write FUserId;

  end;

  function StripQuotes(Source: String): String;

var
  DataModuleMain: TDataModuleMain;

implementation

uses URegisterProfile;

function StripQuotes(Source: String): String;
begin
    Result := Copy(Source, 2, Length(Source) -2);
end;

{$R *.lfm}

{ TDataModuleMain }


procedure TDataModuleMain.DataModuleCreate(Sender: TObject);
begin
  FApiClient := TRTApiClient.Create;
  FApiConfig := TApiConfig.Create;
  FApiConfig.LoadConfigValues;
end;

procedure TDataModuleMain.ActionRegisterProfileExecute(Sender: TObject);
begin
  FormRegisterProfile.ShowModal;
end;

procedure TDataModuleMain.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(FApiClient);
  FreeAndNil(FApiConfig);
end;

function TDataModuleMain.GetLoggedIn: Boolean;
begin
  Result := FApiClient.LoggedIn;
end;

function TDataModuleMain.GetSavedEmail: String;
begin
  Result := FApplicationConfig.GetValue(ResourcesTreeApiClient.ConfigEmailPath, '');
end;


function TDataModuleMain.GetSavedPassword: string;
begin
  Result := FApplicationConfig.GetValue(ResourcesTreeApiClient.ConfigPasswordPath, '');
end;


procedure TDataModuleMain.SetSavedEmail(AValue: String);
begin
  FApplicationConfig.SetValue(ResourcesTreeApiClient.ConfigEmailPath, AValue)
end;

procedure TDataModuleMain.SetSavedPassword(AValue: string);
begin
  FApplicationConfig.SetValue(ResourcesTreeApiClient.ConfigPasswordPath, AValue)
end;

function TDataModuleMain.Login(aEmail: String; aPassword: String): Boolean;
begin
  Result := FApiClient.Login(aEmail, aPassword);
end;

function TDataModuleMain.RegisterProfile(aEmail: String; aPassword: String; aPasswordConfirmation: String;
               aUsername: String; aFirstName: String; aLastName: String; aTaxId: String): Boolean;
begin
  Result := FApiClient.RegisterProfile(
    aEmail, aPassword, aPasswordConfirmation, aUsername, aFirstName, aLastName, aTaxId);
end;

procedure TDataModuleMain.SaveApiConfig;
begin
  FApplicationConfig.Flush;;
end;

function TDataModuleMain.UpdateProfile(aUserId: integer; aUsername: String; aFirstName: String; aLastName: String;
  aTaxId: String): Boolean;
begin
  Result := FApiClient.UpdateProfile(aUserId, aUsername, aFirstName, aLastName, aTaxId);
end;

end.

